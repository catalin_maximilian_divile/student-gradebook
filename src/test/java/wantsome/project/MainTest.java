package wantsome.project;

import org.junit.*;
import wantsome.project.projectdb.DbManager;
import wantsome.project.projectdb.dto.CourseDto;
import wantsome.project.projectdb.dto.GradesDto;
import wantsome.project.projectdb.dto.StudentDto;
import wantsome.project.projectdb.service.dao.CourseDao;
import wantsome.project.projectdb.service.dao.DbInitService;
import wantsome.project.projectdb.service.dao.GradesDao;
import wantsome.project.projectdb.service.dao.StudentDao;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class MainTest {
    private static final String TEST_DB_FILE = "project_test.db";
    private static final List<StudentDto> sampleStudent = Arrays.asList (new StudentDto (-1, "marius", "193444", "123213", "123asda", "0432432"),
            new StudentDto (-1, "andrei", "193444123", "12321333", "123asda23", "0432432asd"),
            new StudentDto (-1, "ciprian", "1827732", "ciprian@yahoo.com", "Vaslui", "0432432"));

    private static final List<CourseDto> sampleCourses = Arrays.asList (new CourseDto (-1,"Mate", 4,"Ciprian"),
            new CourseDto (-1,"Mate3", 7,"Ciprian2"),
            new CourseDto (-1,"Chimie", 2,"Andrei"));

    private static final List<GradesDto> sampleGrades = Arrays.asList (new GradesDto (1,1,3,"Ai picat"),
            new GradesDto (1,2,4,"Ai picat si tu"),new GradesDto (1,3,9,"Bravo"));


    private final StudentDao student = new StudentDao ();
    private final CourseDao course = new CourseDao ();
    private final GradesDao grades = new GradesDao ();

    @BeforeClass
    public static void initDbBeforeAnyTests(){
        DbManager.setDbFile (TEST_DB_FILE);
        DbInitService.createTables ();
    }

    @AfterClass
    public static void deleteDbFileAfterAllTests(){
        new File (TEST_DB_FILE).delete ();
    }

    @Before
    public void insertRowsBeforeTest(){
        assertTrue(student.getAll ().isEmpty ());
        assertTrue ((course.getAll ().isEmpty ()));
        assertTrue (grades.getAll ().isEmpty ());
        for(StudentDto item: sampleStudent){
            student.insert (item);
        }
        for(CourseDto item: sampleCourses){
            course.insert (item);
        }
        for(GradesDto item: sampleGrades){
            grades.insert (item);
        }

    }

    @After
    public void deleteRowsAfterTest(){
        student.getAll ().forEach ((dto->student.delete (dto.getId ())));
        course.getAll ().forEach ((dto->course.delete (dto.getId ())));
        grades.getAll ().forEach ((dto->grades.delete (dto.getStudentId (),dto.getCourseId ())));
        assertTrue (student.getAll ().isEmpty ());
        assertTrue (course.getAll ().isEmpty ());
        assertTrue (grades.getAll ().isEmpty ());
    }

    @Test
    public void getAll(){
        checkOnlyTheSampleItemsArePresentInDb();
    }
    private void assertEqualsItemsExpectId(StudentDto student1, StudentDto student2) {
        assertTrue ("Items should be equal (exceptid): "+ student1+","+student2,
                student1.getName ().equals(student2.getName ())&&student1.getAddress ().equals (student2.getAddress ())
                        &&student1.getCnp ().equals (student2.getCnp ())&&student1.getEmail ().equals (student2.getEmail ())&&
                        student1.getPhone ().equals (student2.getPhone ()));
    }
    private void checkOnlyTheSampleItemsArePresentInDb() {
        List<StudentDto> itemFromDb = student.getAll ();
        assertEquals (3,itemFromDb.size ());
        assertEqualsItemsExpectId(sampleStudent.get (1),itemFromDb.get (0));
        assertEqualsItemsExpectId(sampleStudent.get (0),itemFromDb.get (1));
        assertEqualsItemsExpectId(sampleStudent.get (2),itemFromDb.get (2));

    }


    @Test
    public void get(){
        StudentDto item1FromDb = student.getAll ().get (0);
        assertEqualsItemsExpectId (sampleStudent.get(1), student.get(item1FromDb.getId ()).get());
    }
    @Test
    public void get_forInvalidId(){
        assertFalse(student.get(-99).isPresent ());

    }
    @Test
    public void insert(){
        assertEquals (3,student.getAll ().size ());
        StudentDto newItem = new StudentDto (-1,"Petrica","1978455884","petrica@yahoo.com","Pascani","0768787985");
        student.insert (newItem);
        checkOnlyTheSampleItemsArePresentInDb ();
    }
    @Test
    public void update_forInvalidId(){
        student.update(new StudentDto (-88,"updates","98887889","petru112","iasi","0255565578"));
        checkOnlyTheSampleItemsArePresentInDb ();
    }
     @Test
    public void delete(){
        List<StudentDto> itemsFromDb = student.getAll ();
        student.delete(itemsFromDb.get(0).getId ());
        List<StudentDto> itemsAfterDelete = student.getAll ();
        assertEquals (2,itemsAfterDelete.size ());
        assertEqualsItemsExpectId (itemsFromDb.get(1),itemsAfterDelete.get(0));
        assertEqualsItemsExpectId (itemsFromDb.get(2),itemsAfterDelete.get (1));
     }
     @Test
    public void delete_fromInvalidId(){
        checkOnlyTheSampleItemsArePresentInDb ();
        student.delete (-66);
        checkOnlyTheSampleItemsArePresentInDb ();
     }

}
