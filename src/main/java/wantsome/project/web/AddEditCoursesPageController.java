package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.projectdb.dto.CourseDto;
import wantsome.project.projectdb.service.dao.CourseDao;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class AddEditCoursesPageController {

    private static final CourseDao courseDao = new CourseDao();

    public static String showAddFrom(Request req, Response res) {
        return renderAddUpdateForm("", "", "", "", null);
    }

    public static String showUpdateForm(Request req, Response res) {
        String id = req.params("id");

        Optional<CourseDto> optCourse = courseDao.get(Integer.parseInt(id));

        if (optCourse.isPresent()) {
            CourseDto course = optCourse.get();
            return renderAddUpdateForm (String.valueOf(course.getId()),
                    course.getDescription(),
                    String.valueOf(course.getNumberOfCredits()),
                    course.getCourseHolder(), null);
        }
        throw new RuntimeException("Course " + id + " not found!");
    }

    private static String renderAddUpdateForm(String id, String description, String numberOfCredits, String courseHolder, String errorMessage) {
        Map<String, Object> model = new HashMap<>();
        model.put("prevId", id);
        model.put("previousDescription", description);
        model.put("previousNumberOfCredits", numberOfCredits);
        model.put("previousCourseHolder", courseHolder);
        model.put("errorMsg", errorMessage);
        model.put("isUpdate", id != null && !id.isEmpty());
        return render(model, "add_edit_courses.vm");
    }

    public static Object handleAddUpdateRequest(Request req, Response res) {
        String id = req.queryParams("id");
        String description = req.queryParams("description");
        String numberOfCredits = req.queryParams("numberOfCredits");
        String courseHolder = req.queryParams("courseHolder");


        try {
            CourseDto course = validateAndBuildCourses(id, description, numberOfCredits, courseHolder);
            if (id != null && !id.isEmpty()) {
                courseDao.update(course);
            } else {
                courseDao.insert(course);
            }
            res.redirect ("/courses");
            return res;
        } catch(Exception e) {
            return renderAddUpdateForm(id, description, numberOfCredits, courseHolder, e.getMessage ());

        }
    }

    private static CourseDto validateAndBuildCourses(String id, String description, String numberOfCredits, String courseHolder) {
        long idValue = id != null && !id.isEmpty() ? Long.parseLong(id) : -1;
        int credits = numberOfCredits !=null && !numberOfCredits.isEmpty() ? Integer.parseInt(numberOfCredits) : 0;
        return new CourseDto(idValue, description, credits, courseHolder);
    }
}
