package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.projectdb.dto.GradesFullDetailsDto;
import wantsome.project.projectdb.service.dao.GradesDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static wantsome.project.web.SparkUtil.render;

public class GradesPageController {
    private static final GradesDao gradesDao = new GradesDao ();

    public static String showGradesPage(Request req, Response res) {
        return renderGradesPage(null);
    }

    private static String renderGradesPage(String errorMsg) {
        List<GradesFullDetailsDto> grades = gradesDao.getAll();
        Map<String, Object> model = new HashMap<> ();
        model.put("grades", grades);
        model.put("errorMsg", errorMsg);
        return render(model, "grades.vm");
    }

    public static Object handleDeleteRequest(Request req, Response res) {
        String studentId = req.params("studentId");
        String courseId = req.params("courseId");
        try {
            gradesDao.delete(Long.parseLong(studentId), Long.parseLong(courseId));
            res.redirect("/grades");
            return res;
        } catch (Exception e) {
            String errorMsg = e.getMessage().contains ("Abort due to constraint violation (FOREIGN KEY constraint failed") ?
                    "Error: cannot delete grade" + studentId + " due to existing grades" : e.getMessage();
            return renderGradesPage(errorMsg);
        }

    }
}
