package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.projectdb.dto.StudentDto;
import wantsome.project.projectdb.service.dao.StudentDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static wantsome.project.web.SparkUtil.render;

public class StudentsPageController {

    private static final StudentDao studentDao = new StudentDao ();

    public static String showStudentsPage(Request req, Response res){

        List<StudentDto> allStudents = studentDao.getAll();
        long studentsCount = allStudents.size();
        Map<String, Object> model =new HashMap<>();
        model.put("students", allStudents);
        model.put("studentsCount", studentsCount);
        return render(model,"students.vm");
    }

    public static Object handleDeleteRequest(Request req, Response res){
        String id = req.params("id");
        studentDao.delete(Long.parseLong (id));
        res.redirect("/students");
        return res;
    }
}
