package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.projectdb.dto.StudentDto;
import wantsome.project.projectdb.service.dao.StudentDao;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class AddEditStudentsPageController {
    private static final StudentDao studentDao = new StudentDao();

    public static String showAddFrom(Request req, Response res) {
        return renderAddUpdateForm("", "", "", "", "", "", null);
    }

    public static String showUpdateForm(Request req, Response res) {
        String id = req.params ("id");
        Optional<StudentDto> optStudent = studentDao.get(Integer.parseInt (id));
        if (optStudent.isPresent()) {
            StudentDto stud = optStudent.get();
            return renderAddUpdateForm(String.valueOf (stud.getId()), stud.getName(), stud.getCnp(), stud.getEmail(), stud.getAddress(), stud.getPhone(), "");
        }
        throw new RuntimeException("Student " + id + " not found!");
    }

    private static String renderAddUpdateForm(String id, String studentName, String cnp, String email, String address, String phoneNumber, String errorMessage) {
        Map<String, Object> model = new HashMap<> ();
        model.put("prevId", id);
        model.put("previousName", studentName);
        model.put("previousCnp", cnp);
        model.put("previousEmail", email);
        model.put("previousAddress", address);
        model.put("previousPhoneNumber", phoneNumber);
        model.put("errorMsg", errorMessage);
        model.put("isUpdate", id != null && !id.isEmpty());
        return render (model, "add_edit_students.vm");
    }

    public static Object handleAddUpdateRequest(Request req, Response res) {
        String id = req.queryParams("id");
        String studentName = req.queryParams("studentName");
        String cnp = req.queryParams("cnp");
        String email = req.queryParams("email");
        String address = req.queryParams("address");
        String phoneNumber = req.queryParams("phoneNumber");

        try {
            StudentDto student = validateAndBuildStudents(id, studentName, cnp, email, address, phoneNumber);
            if (id != null && !id.isEmpty()) {
                studentDao.update(student);
            } else {
                studentDao.insert(student);
            }
            res.redirect("/students");
            return res;
        } catch (Exception e) {
            String errorMsg = e.getMessage().contains("UNIQUE constraint failed: STUDENTS.CNP") ?
                    "Error: student with same CNP already exists!" :
                    e.getMessage();
            return renderAddUpdateForm(id, studentName, cnp, email, address, phoneNumber, errorMsg);

        }
    }

    private static StudentDto validateAndBuildStudents(String id, String studentName, String cnp, String email, String address, String phoneNumber) {
        long idValue = id != null && !id.isEmpty() ? Long.parseLong(id) : -1;
        if (studentName ==null|| studentName.isEmpty()){
            throw new RuntimeException("Name is required!");
        }
        if (cnp ==null||cnp.isEmpty()){
            throw new RuntimeException("Cnp is required!");
        }
        if (cnp.length()!=13){
            throw new RuntimeException("CNP must have 13 characters!");
        }
        if (email ==null||email.isEmpty()){
            throw new RuntimeException("Email is required!");
        }
        if (address ==null||address.isEmpty()){
            throw new RuntimeException("Address is required!");
        }
        if (phoneNumber ==null||phoneNumber.isEmpty()){
            throw new RuntimeException("Phone number is required!");
        }
        if (phoneNumber.length()!=10){
            throw new RuntimeException("Phone number must have 10 characters!");
        }
        return new StudentDto(idValue, studentName, cnp, email, address, phoneNumber);
    }
}
