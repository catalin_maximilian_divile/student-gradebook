package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.projectdb.dto.CourseDto;
import wantsome.project.projectdb.service.dao.CourseDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static wantsome.project.web.SparkUtil.render;

public class CoursesPageController {

    private static final CourseDao courseDao = new CourseDao();

    public static String showCoursesPage(Request req, Response res){

        List<CourseDto> allCourses = courseDao.getAll();
        long coursesCount = allCourses.size();
        Map<String,Object> model = new HashMap<>();
        model.put("courses", allCourses);
        model.put("coursesCount", coursesCount);
        return render(model,"courses.vm");
    }

    public static Object handleDeleteRequest(Request req, Response res){
        String id = req.params("id");
        courseDao.delete(Long.parseLong (id));
        res.redirect("/courses");
        return res;
    }
}
