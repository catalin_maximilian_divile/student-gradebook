package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.projectdb.dto.GradesDto;
import wantsome.project.projectdb.service.dao.CourseDao;
import wantsome.project.projectdb.service.dao.GradesDao;
import wantsome.project.projectdb.service.dao.StudentDao;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class EditGradesPageController{

    private static final GradesDao gradesDao = new GradesDao();
    private static final StudentDao studentDao = new StudentDao();
    private static final CourseDao courseDao = new CourseDao();

    public static String showUpdateForm(Request req, Response res){

        String studentId = req.params("studentId");
        String courseId = req.params("courseId");

        Optional<GradesDto> optGrades = gradesDao.get(Integer.parseInt(studentId), Integer.parseInt(courseId));

        if (!optGrades.isPresent()) {
            throw new RuntimeException("Grade with student id " + studentId + " not found!");
        }

        GradesDto grades = optGrades.get();
        return renderUpdateForm(
                String.valueOf(grades.getStudentId()),
                String.valueOf(grades.getCourseId()),
                String.valueOf(grades.getGrade()),
                grades.getDetails(),
                "");
    }

    private static String renderUpdateForm(String studentId,
                                           String courseId,
                                           String grade,
                                           String details,
                                           String errorMessage) {

        Map<String, Object> model = new HashMap<> ();

        model.put("prevStudentId", studentId);
        model.put("previousCourseId", courseId);
        model.put("previousGrade", grade);
        model.put("previousDetails", details);
        model.put("errorMsg", errorMessage);
        model.put("students", studentDao.getAll());
        model.put("courses", courseDao.getAll());

        return render(model, "edit_grades.vm");
    }

    public static Object handleUpdateRequest(Request req, Response res) {
        String studentId = req.queryParams("studentId");
        String courseId = req.queryParams("courseId");
        String grade = req.queryParams("grade");
        String details = req.queryParams("details");

        try {
            GradesDto grades = validateAndBuildGrades(studentId, courseId, grade, details);
            gradesDao.update(grades);

            res.redirect("/grades");
            return res;

        } catch (Exception e) {
            return renderUpdateForm(studentId, courseId, grade, details, e.getMessage());
        }
    }

    private static GradesDto validateAndBuildGrades(String studentId, String courseId, String grade, String details) {
        long studentIdValue = Long.parseLong(studentId);
        long courseIdValue = Long.parseLong(courseId);
        double gradeValue = Double.parseDouble(grade);

        if(gradeValue<0 || gradeValue>10){
            throw new RuntimeException ("Grade must be between 0 and 10!");
        }

        if (details == null || details.isEmpty()) {
            throw new RuntimeException("Details are required!");
        }
        return new GradesDto(studentIdValue, courseIdValue, String.valueOf(gradeValue), details);
    }
}
