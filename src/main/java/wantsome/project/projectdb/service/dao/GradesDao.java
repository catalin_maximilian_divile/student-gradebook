package wantsome.project.projectdb.service.dao;

import wantsome.project.projectdb.DbManager;
import wantsome.project.projectdb.dto.GradesDto;
import wantsome.project.projectdb.dto.GradesFullDetailsDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GradesDao {

    public List<GradesFullDetailsDto> getAll() {

        String sql = "SELECT A.*, S.STUDENT_NAME AS NAME," +
                " C.DESCRIPTION AS COURSE" +
                " FROM GRADES A" +
                " INNER JOIN STUDENTS S ON A.STUDENT_ID = S.ID" +
                " INNER JOIN COURSES C ON A.COURSE_ID = C.ID" +
                " ORDER BY S.ID ";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            List<GradesFullDetailsDto> groupGradesList = new ArrayList<>();
            while (rs.next()) {
                groupGradesList.add(new GradesFullDetailsDto (
                        rs.getLong("STUDENT_ID"),
                        rs.getLong("COURSE_ID"),
                        rs.getString("NAME"),
                        rs.getString("COURSE"),
                        rs.getString("GRADE"),
                        rs.getString("DETAILS")));
            }
            return groupGradesList;
        } catch (SQLException e) {
            throw new RuntimeException("Error loading all grades: " + e.getMessage());
        }
    }


    /**
     * Load one specific grade from DB (by id)
     */
    public Optional<GradesDto> get( long studentId, long courseId) {

        String sql = "SELECT * FROM GRADES WHERE (STUDENT_ID = ? AND COURSE_ID = ?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, studentId);
            ps.setLong(2,courseId);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {

                    return Optional.of (new GradesDto(
                            rs.getLong("STUDENT_ID"),
                            rs.getLong("COURSE_ID"),
                            rs.getString("GRADE"),
                            rs.getString("DETAILS")));
                }
                return Optional.empty(); //default value
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading grade for student id " + studentId + " : " + e.getMessage());
        }
    }

    /**
     * Add a new grade to DB
     */
    public void insert(GradesDto grades) {

        String sql = "INSERT INTO GRADES " +
                "(STUDENT_ID," +
                " COURSE_ID," +
                " GRADE," +
                " DETAILS)" +
                " VALUES(?,?,?,?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, grades.getStudentId());
            ps.setLong(2, grades.getCourseId());
            ps.setString(3, grades.getGrade());
            ps.setString(4, grades.getDetails());

            ps.execute();

        } catch (SQLException e) {

            throw new RuntimeException("Error inserting in db grade " + grades.getGrade() + " : " + e.getMessage());
        }
    }

    /**
     * Update an existing grade in DB
     */
    public void update(GradesDto grades) {

        String sql = "UPDATE GRADES " +
                "SET STUDENT_ID = ? ," +
                " COURSE_ID = ?," +
                " GRADE = ?, " +
                " DETAILS = ?" +
                " WHERE (STUDENT_ID = ? AND COURSE_ID = ?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, grades.getStudentId());
            ps.setLong(2, grades.getCourseId());
            ps.setString(3, grades.getGrade());
            ps.setString(4, grades.getDetails());
            ps.setLong(5, grades.getStudentId());
            ps.setLong(6, grades.getCourseId());

            ps.executeUpdate();

        } catch (SQLException e) {

            throw new RuntimeException("Error while updating grade " + grades + " : " + e.getMessage());

        }
    }

    /**
     * Delete a grade from DB (by student id and course id)
     */
    public void delete(long studentId, long courseId) {

        String sql = "DELETE FROM GRADES" +
                " WHERE (STUDENT_ID = ? AND COURSE_ID = ?) ";

        try (Connection conn = DbManager.getConnection ();
             PreparedStatement ps = conn.prepareStatement (sql)) {


            ps.setLong(1,studentId);
            ps.setLong(2,courseId);

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException ("Error while deleting grade for student with id " + studentId + ": " + e.getMessage());
        }
    }
}
