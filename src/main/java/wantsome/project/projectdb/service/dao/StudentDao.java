package wantsome.project.projectdb.service.dao;

import wantsome.project.projectdb.DbManager;
import wantsome.project.projectdb.dto.StudentDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StudentDao {
    public List<StudentDto> getAll() {

        String sql = "SELECT * FROM STUDENTS ORDER BY ID";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            List<StudentDto> studentList = new ArrayList<>();
            while (rs.next()) {
                studentList.add(extractStudentsFromResult(rs));
            }
            return studentList;
        } catch (SQLException e) {
            throw new RuntimeException("Error loading all students: " + e.getMessage());
        }


    }

    private StudentDto extractStudentsFromResult(ResultSet rs) throws SQLException {

        return new StudentDto (rs.getLong("ID"),
                rs.getString("STUDENT_NAME"),
                rs.getString("CNP"),
                rs.getString("EMAIL"),
                rs.getString("ADDRESS"),
                rs.getString("PHONE_NUMBER"));
    }

    /**
     * Load one specific student from DB (by id)
     */
    public Optional<StudentDto> get(long id) {

        String sql = "SELECT * FROM STUDENTS WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    StudentDto studentDto = extractStudentsFromResult(rs);
                    return Optional.of(studentDto);
                }
                return Optional.empty(); //default value
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading student with id " + id + " : " + e.getMessage());
        }
    }

    /**
     * Add a new student to DB
     */
    public void insert(StudentDto studentDto) {

        String sql = "INSERT INTO STUDENTS " +
                " (STUDENT_NAME , " +
                " CNP ," +
                " EMAIL ," +
                " ADDRESS , " +
                " PHONE_NUMBER)" +
                " VALUES(?,?,?,?,?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, studentDto.getName());
            ps.setString(2, studentDto.getCnp());
            ps.setString(3, studentDto.getEmail());
            ps.setString(4, studentDto.getAddress());
            ps.setString(5, studentDto.getPhone());
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error inserting in db student " + studentDto + " : " + e.getMessage());

        }
    }

    /**
     * Update an existing student in DB
     */
    public void update(StudentDto studentDto) {

        String sql = "UPDATE STUDENTS " +
                " SET STUDENT_NAME = ?," +
                " CNP = ?," +
                " EMAIL =?," +
                " ADDRESS = ?," +
                " PHONE_NUMBER = ?" +
                " WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, studentDto.getName());
            ps.setString(2, studentDto.getCnp());
            ps.setString(3, studentDto.getEmail());
            ps.setString(4, studentDto.getAddress());
            ps.setString(5, studentDto.getPhone());
            ps.setLong(6, studentDto.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed: STUDENTS.CNP") ?
                    "Student with same CNP already exists" :
                    e.getMessage();
            throw new RuntimeException("Error while updating student " + studentDto.getCnp() + " : " + details);
        }
    }

    /**
     * Delete a student from DB (by id)
     */
    public void delete(long id) {

        String sql = "DELETE FROM STUDENTS WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);
            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("FOREIGN KEY constraint failed") ?
                    "Student is used by and existing grade" :
                    e.getMessage();
            throw new RuntimeException("Error while deleting student " + id + ": " + details);
        }
    }
}
