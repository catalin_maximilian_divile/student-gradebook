package wantsome.project.projectdb.service.dao;

import wantsome.project.projectdb.DbManager;
import wantsome.project.projectdb.dto.CourseDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CourseDao {
    public List<CourseDto> getAll() {

        String sql = "SELECT * FROM COURSES ORDER BY ID";

        try(Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            List<CourseDto> coursesList = new ArrayList<>();
            while (rs.next()) {
                coursesList.add(extractCoursesFromResult(rs));
            }
            return coursesList;
        } catch(SQLException e) {
            throw new RuntimeException("Error loading courses: " + e.getMessage());
        }

    }

    private CourseDto extractCoursesFromResult(ResultSet rs) throws SQLException {

        return new CourseDto(rs.getLong("ID"),
                rs.getString("DESCRIPTION"),
                rs.getInt("NUMBER_OF_CREDITS"),
                rs.getString("COURSE_HOLDER"));
    }

    /**
     * Load one specific course from DB (by id)
     */
    public Optional<CourseDto> get(long id) {

        String sql = "SELECT * FROM COURSES WHERE ID = ?";

        try(Connection conn = DbManager.getConnection ();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    CourseDto courseDto = extractCoursesFromResult(rs);
                    return Optional.of(courseDto);
                }
                return Optional.empty(); //default value
            }
        } catch(SQLException e) {
            throw new RuntimeException("Error loading course: " + e.getMessage());
        }

    }

    /**
     * Add a new course to DB
     */
    public void insert(CourseDto courseDto) {

        String sql = "INSERT INTO COURSES " +
                "(DESCRIPTION, " +
                "NUMBER_OF_CREDITS, " +
                "COURSE_HOLDER)" +
                " VALUES(?,?,?)";

        try(Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, courseDto.getDescription());
            ps.setInt(2, courseDto.getNumberOfCredits());
            ps.setString(3, courseDto.getCourseHolder());
            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed: COURSES.DESCRIPTION") ?
                    "Course with same description already exists" :
                    e.getMessage();

            throw new RuntimeException("Error inserting in db course " + courseDto + " : " + details);
        }
    }

    /**
     * Update an existing course in DB
     */
    public void update(CourseDto courseDto) {

        String sql = "UPDATE COURSES " +
                "SET DESCRIPTION = ?," +
                " NUMBER_OF_CREDITS = ?," +
                " COURSE_HOLDER = ?" +
                " WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, courseDto.getDescription());
            ps.setInt(2, courseDto.getNumberOfCredits());
            ps.setString(3, courseDto.getCourseHolder());
            ps.setLong(4, courseDto.getId());
            ps.executeUpdate();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed: COURSES.DESCRIPTION") ?
                    "Course with same description already exists" :
                    e.getMessage();
            throw new RuntimeException("Error updating course " + courseDto.getId() + " : " + details);
        }
    }

    /**
     * Delete a course from DB (by id)
     */
    public void delete(long id) {

        String sql = "DELETE FROM COURSES WHERE ID = ?";

        try(Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);
            ps.execute();

        } catch(SQLException e) {
            String details = e.getMessage().contains("FOREIGN KEY constraint failed") ?
                    "Course is used by existing students" :
                    e.getMessage();
            throw new RuntimeException("Error while deleting courses " + id + ": " + details);
        }
    }
}
