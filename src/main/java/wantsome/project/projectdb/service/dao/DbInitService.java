package wantsome.project.projectdb.service.dao;

import wantsome.project.projectdb.DbManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * Initializes the DB as needed - creates missing tables, etc..
 * Should be called once, on app startup.
 */
public class DbInitService {


    private static final String CREATE_STUDENTS_SQL = "CREATE TABLE IF NOT EXISTS STUDENTS (\n" +
            "ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "STUDENT_NAME TEXT NOT NULL,\n" +
            "CNP TEXT NOT NULL UNIQUE,\n" +
            "EMAIL TEXT NOT NULL,\n" +
            "ADDRESS TEXT NOT NULL,\n" +
            "PHONE_NUMBER TEXT NOT NULL\n" +
            ");";

    private static final String CREATE_COURSES_SQL = "CREATE TABLE IF NOT EXISTS COURSES (\n" +
            "ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "DESCRIPTION TEXT NOT NULL UNIQUE,\n" +
            "NUMBER_OF_CREDITS INTEGER NOT NULL,\n" +
            "COURSE_HOLDER TEXT NOT NULL\n" +
            ");";


    private static final String CREATE_GRADES_SQL = "CREATE TABLE IF NOT EXISTS GRADES (\n" +
            "STUDENT_ID TEXT REFERENCES STUDENTS(ID) NOT NULL,\n" +
            "COURSE_ID TEXT REFERENCES COURSES(ID) NOT NULL ,\n" +
            "GRADE REAL NOT NULL,\n" +
            "DETAILS TEXT," +
            " CONSTRAINT COURSE_ID_UNIQUE UNIQUE (STUDENT_ID, COURSE_ID)" +
            ");";

    public static void createTables() {
        createMissingTables ();
    }

    private static void createMissingTables() {
        try (Connection conn = DbManager.getConnection ();
             Statement st = conn.createStatement ()) {
            st.execute (CREATE_STUDENTS_SQL);
            st.execute (CREATE_COURSES_SQL);
            st.execute (CREATE_GRADES_SQL);

        } catch (SQLException e) {
            System.err.println ("Error creating missing tables: " + e.getMessage ());
        }
    }

}
