package wantsome.project.projectdb.dto;

import java.util.Objects;

public class GradesDto {

    private final long studentId;
    private final long courseId;
    private final String grade;
    private final String details;


    public GradesDto(long studentId, long courseId, String grade, String details) {
        this.studentId = studentId;
        this.courseId = courseId;
        this.grade = grade;
        this.details = details;

    }

    public long getStudentId() {
        return studentId;
    }

    public long getCourseId() {
        return courseId;
    }

    public String getGrade() {
        return grade;
    }

    public String getDetails() {
        return details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GradesDto gradesDto = (GradesDto) o;
        return studentId == gradesDto.studentId &&
                courseId == gradesDto.courseId &&
                Objects.equals(grade, gradesDto.grade) &&
                Objects.equals(details, gradesDto.details);
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentId, courseId, grade, details);
    }

    @Override
    public String toString() {
        return "GradesDto{" +
                "studentId=" + studentId +
                ", courseId=" + courseId +
                ", grade=" + grade +
                ", details='" + details + '\'' +
                '}';
    }
}
