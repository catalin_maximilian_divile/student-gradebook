package wantsome.project.projectdb.dto;

import java.util.Objects;

public class StudentDto {


    private final long id;
    private final String name;
    private final String cnp;
    private final String email;
    private final String address;
    private final String phone;


    public StudentDto(long id, String name, String cnp,
                      String email, String address, String phone) {
        this.id = id;
        this.name = name;
        this.cnp = cnp;
        this.email = email;
        this.address = address;
        this.phone = phone;

    }

    public StudentDto(String name, String cnp,
                      String email, String address, String phone) {
        this(-1, name, cnp, email, address, phone);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCnp() {
        return cnp;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass () != o.getClass ()) return false;
        StudentDto that = (StudentDto) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(cnp, that.cnp) &&
                Objects.equals(email, that.email) &&
                Objects.equals(address, that.address) &&
                Objects.equals(phone, that.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, cnp, email, address, phone);
    }

    @Override
    public String toString() {
        return "StudentDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cnp='" + cnp + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}

