package wantsome.project.projectdb.dto;

import java.util.Objects;

public class GradesFullDetailsDto {
    private final long studentId;
    private final String studentName;
    private final long courseId;
    private final String courseName;
    private final String grade;
    private final String details;


    public GradesFullDetailsDto(long studentId, long courseId, String studentName, String courseName, String grade, String details){
        this.studentId = studentId;
        this.studentName = studentName;
        this.courseName = courseName;
        this.courseId = courseId;
        this.grade = grade;
        this.details = details;

    }

    public long getStudentId(){
        return studentId;
    }
    public String getStudentName() {
        return studentName;
    }

    public String getCourseName() {
        return courseName;
    }

    public long getCourseId() {
        return courseId;
    }

    public String getGrade() {
        return grade;
    }

    public String getDetails() {
        return details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GradesFullDetailsDto that = (GradesFullDetailsDto) o;
        return studentId == that.studentId &&
                courseId == that.courseId &&
                Objects.equals(studentName, that.studentName) &&
                Objects.equals(courseName, that.courseName) &&
                Objects.equals(grade, that.grade) &&
                Objects.equals(details, that.details);
    }

    @Override
    public int hashCode() {
        return Objects.hash (studentId, studentName, courseId, courseName, grade, details);
    }

    @Override
    public String toString() {
        return "StudentEnrolmentDetailsDto{" +
                "studentId=" + studentId +
                ", studentName='" + studentName + '\'' +
                ", courseId=" + courseId +
                ", courseName='" + courseName + '\'' +
                ", grade=" + grade +
                ", details='" + details + '\'' +
                '}';
    }
}
