package wantsome.project.projectdb.dto;

import java.util.Objects;

public class CourseDto {

    private final long id;
    private final String description;
    private final int numberOfCredits;
    private final String courseHolder;

    public CourseDto(long idOfCourse, String description, int numberOfCredits, String courseHolder) {
        this.id = idOfCourse;
        this.description = description;
        this.numberOfCredits = numberOfCredits;
        this.courseHolder = courseHolder;
    }

    public CourseDto(String description, int numberOfCredits, String courseHolder) {
        this (-1, description, numberOfCredits, courseHolder);
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public int getNumberOfCredits() {
        return numberOfCredits;
    }

    public String getCourseHolder() {
        return courseHolder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CourseDto courseDto = (CourseDto) o;
        return id == courseDto.id &&
                numberOfCredits == courseDto.numberOfCredits &&
                Objects.equals(description, courseDto.description) &&
                Objects.equals(courseHolder, courseDto.courseHolder);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, numberOfCredits, courseHolder);
    }

    @Override
    public String toString() {
        return "CourseDto{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", numberOfCredits=" + numberOfCredits +
                ", courseHolder='" + courseHolder + '\'' +
                '}';
    }
}
