package wantsome.project;

import spark.Spark;
import wantsome.project.projectdb.service.dao.DbInitService;
import wantsome.project.web.*;

import static spark.Spark.*;

/**
 * Main class of the application
 */
public class StudentsGradebookWebApp {

    public static void main(String[] args) {
        
        DbInitService.createTables();
        staticFileLocation("/public");
        configureRoutesAndStart();
        Spark.awaitInitialization();
        System.out.println("Web app started, url : http://localhost:4567/students");
    }


    private static void configureRoutesAndStart() {
        get("/students", StudentsPageController::showStudentsPage);

        get("/add", AddEditStudentsPageController::showAddFrom);
        post("/add", AddEditStudentsPageController::handleAddUpdateRequest);

        get("/update/:id", AddEditStudentsPageController::showUpdateForm);
        post("/update/:id", AddEditStudentsPageController::handleAddUpdateRequest);

        get("/delete/:id", StudentsPageController::handleDeleteRequest);
        //--Courses
        get("/courses", CoursesPageController::showCoursesPage);

        get("/add_course", AddEditCoursesPageController::showAddFrom);
        post("/add_course",AddEditCoursesPageController::handleAddUpdateRequest);

        get("/update_course/:id", AddEditCoursesPageController::showUpdateForm);
        post("/update_course/:id", AddEditCoursesPageController::handleAddUpdateRequest);

        get("/delete_course/:id", CoursesPageController::handleDeleteRequest);

        //--Grades
        get("/grades", GradesPageController::showGradesPage);

        get("/add_grade",AddGradesPageController::showAddFrom);
        post("/add_grade",AddGradesPageController::handleAddRequest);

        get("/update_grades/:studentId/:courseId",EditGradesPageController::showUpdateForm);
        post("/update_grades/:studentId/:courseId",EditGradesPageController::handleUpdateRequest);

        get("/delete_grade/:studentId/:courseId",GradesPageController::handleDeleteRequest);

        exception(Exception.class, ErrorPageController::handleException);

    }
}